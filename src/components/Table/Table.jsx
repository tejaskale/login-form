import React, { Fragment, useState } from 'react'
import "./table-styles.css"

function Table(props) {
   let allData=props.alldata
   let [newdata,setNewdata] = useState([])
   console.log(allData);
  return (
    <Fragment>
       <div className='table-container'>
       <table>
            <thead>
              <tr>
              <th>id</th>
              <th>First name</th>
              <th>Last Name</th>
              <th>Gender</th>
              <th>Semister</th>
              <th>Hobby</th>
              <th>Action</th>
              </tr>
            </thead>
            <tbody>
                {allData.map((current,index)=>{
                    return(
                        <tr key={index}>
                        <td>{index + 1}</td>
                        <td>{current.fname}</td>
                        <td>{current.lname}</td>
                        <td>{current.gen}</td>
                        <td>{current.sem}</td>
                        <td>{current.hobby}</td>
                        <td>
                            <button>Edit</button>
                            <button onClick={(e)=>{props.alldata.filter((current,delindex)=>{
                                if(delindex!==index){
                                    props.alldata.push(current);
                                }
                                
                                   
                                
                            })}} >Delete</button>
                        </td>
                    </tr>
                    )
                })
                
                }
            </tbody>
        </table>
       </div>
        <button onClick={()=>props.setOpentable(false)} >Go to form</button>
    </Fragment>
  )
}

export default Table