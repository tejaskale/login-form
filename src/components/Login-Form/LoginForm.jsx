import React, { Fragment, useState } from 'react'
import Table from '../Table/Table'
import "./LoginForm-styles.css"

function LoginForm() {
    let[alldata,setAlldata] =useState([])
    let [empInfo,setEmpInfo] = useState({})
    let [firstName,setFirstName] = useState(null)
    let [lastName,setLastName] = useState(null)
    let [gender,setGender] = useState(null)
    let [semister,setSemister] = useState(null)
    let [hobby,setHobby] = useState(null)
    let [openTable,setOpentable] = useState(false)


    function handleClickOnSave() {
        setEmpInfo({
            fname:firstName,
            lname:lastName,
            gen:gender,
            sem:semister,
            hobby:hobby
        });
        setAlldata([...alldata,empInfo]);
    }

  return (
<Fragment>
 
    {
        openTable?<Table alldata={alldata} setOpentable={setOpentable}/>:   <div className='wrapper'>
        <div className="container">
            <div className='inp-container' >
                <p className='inp-info-text' >
                    First name
                </p>
                <input type="text" onChange={(e)=>{setFirstName(e.target.value)}} className='text-type-inp' />
            </div>

            <div className='inp-container'>
                <p className='inp-info-text' >
                        Last name
                </p>
                <input type="text" onChange={(e)=>{setLastName(e.target.value)}} className='text-type-inp' />
            </div>

            <div className='inp-container'>
                <p className='inp-info-text' >
                   Gender
                </p>
                <input type="radio" onChange={(e)=>{setGender(e.target.value)}} className='gender-inp' name='gender' value='male' id='male' /> <label htmlFor="male">Male</label>
                <input type="radio" onChange={(e)=>{setGender(e.target.value)}} className='gender-inp'name='gender' value='female' id='female' /> <label htmlFor="female">Female</label>
            </div>

            <div className='inp-container'>
                <p className='inp-info-text' >
                    Semester
                </p>
                <input type="text" onChange={(e)=>{setSemister(e.target.value)}} className='text-type-inp' />
            </div>

            <div className='inp-container'>
                <p className='inp-info-text' >
                    Hobby
                </p>
                <input type="checkbox" onChange={(e)=>{setHobby(e.target.value)}} className='inp-hobby' name='hobby' value="cricket" id='cricket' /> <label htmlFor="cricket">cricket</label>
                <input type="checkbox" onChange={(e)=>{setHobby(e.target.value)}} className='inp-hobby' name='hobby' value="coding" id='coding' /> <label htmlFor="coding">Coding</label>
                <input type="checkbox" onChange={(e)=>{setHobby(e.target.value)}} className='inp-hobby' name='hobby' value="travelling" id='travelling' /> <label htmlFor="travelling">Travelling</label>
            </div>

            <div className='inp-container'> 
                <button className="btn" onClick={handleClickOnSave}>Save</button>
                <button className="btn">Cancel</button>
                <button onClick={()=>setOpentable(true)} className='table-btn' >Go to Table</button>
            </div>
            
        </div>
    </div>
    }
</Fragment>
  )
}

export default LoginForm